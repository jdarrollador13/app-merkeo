import Conection from '../loaders/databaseLoader'
//import * as sql from 'mssql'
import { Inject } from "typescript-ioc";

/**
 * 
 * @category DAO
 */
export class ProductosDAO {

	constructor(
		@Inject private databaseConnection: Conection
	) {
		// code...
	}
	/**
	@router http://localhost:3001/api/productos/listado
    **/
	public async obtenerProductos(): Promise<void> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT T1."IdProducto" , T1."NombreProd", T1."Precio", 
														T1."Unidades" - T2."Unidades" "Unidades", T1."Fecha"
													  FROM
														(SELECT "PM"."IdProducto" , "PM"."NombreProd", "PM"."Precio", 
														"PM"."Unidades", "PM"."Fecha"
														FROM "PRODUCTO_MER" "PM")T1
													  INNER JOIN 
														(SELECT CASE  WHEN  ("PM"."Unidades"  - "PC"."cantidad")::TEXT IS  NULL 
														THEN  0 ELSE "PC"."cantidad"   END "Unidades", 
														"PM"."Fecha", "PM"."IdProducto"
														FROM "PRODUCTO_MER" "PM"
													  LEFT JOIN "PEDIDO_PRODUCTO_CLIENTE_MER" "PC" 
													  ON "PM"."IdProducto" = "PC"."IdProducto")T2
													  ON T1."IdProducto" = T2."IdProducto";`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'inventory' : query.rows} 
			}else{
				data = { 'code' :201, 'inventory' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}


	/**
	@router http://localhost:3001/api/productos/listado/conductor
    **/
	public async obtenerProductosConductor(): Promise<void> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT PM.cantidad, PM."FachaPedido", 
														PM."IdConductor",
														CL."NombreCliente", CL."telCliente", CL."DirCliente",
														PR."NombreProd", PR."Precio",
														CM."NombreConductor", CM."IdentificacionConductor"
													  FROM "PEDIDO_PRODUCTO_CLIENTE_MER" PM
													  INNER JOIN "CLIENTE_MER" CL ON PM."IdCliente" = CL."IdCliente"
												      INNER JOIN "PRODUCTO_MER" PR ON PM."IdProducto" = PR."IdProducto"
													  INNER JOIN "CONDUCTORES_MER" CM ON PM."IdConductor" = CM."IdConductor";`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'orders' : query.rows} 
			}else{
				data = { 'code' :201, 'orders' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}


	/**
	@router http://localhost:3001/api/productos/menos/vendido
    **/
	public async obtenerProductosMenosVendido(): Promise<void> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT T1."TotalProd", T1."NombreProd", T1."FachaPedido"
														FROM
														(SELECT  SUM(PC.cantidad) "TotalProd", PC."IdProducto", PC."FachaPedido", PM."NombreProd"
														FROM "PEDIDO_PRODUCTO_CLIENTE_MER" PC
														INNER JOIN "PRODUCTO_MER" PM ON PC."IdProducto" = PM."IdProducto"
														WHERE PC."FachaPedido" = '2020-03-01'
														GROUP BY PC."IdProducto", PM."NombreProd", PC."FachaPedido")T1
														ORDER BY T1."TotalProd" ASC LIMIT 1;`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	/**
	@router http://localhost:3001/api/productos/listado/:id
    **/
	public async obtenerProductosId(id:number|any): Promise<void> {
		console.log(id,'id')
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT T1."IdProducto" , T1."NombreProd", T1."Precio", 
														T1."Unidades" - T2."Unidades" "Unidades", T1."Fecha",
														T2."IdPedidos"
													  FROM
														(SELECT "PM"."IdProducto" , "PM"."NombreProd", "PM"."Precio", 
														"PM"."Unidades", "PM"."Fecha"
														FROM "PRODUCTO_MER" "PM")T1
														INNER JOIN 
														(SELECT CASE  WHEN  ("PM"."Unidades"  - "PC"."cantidad")::TEXT IS  NULL 
														THEN  0 ELSE "PC"."cantidad"   END "Unidades", 
														"PM"."Fecha", "PM"."IdProducto", "PC"."IdPedidos"
														FROM "PRODUCTO_MER" "PM"
														LEFT JOIN "PEDIDO_PRODUCTO_CLIENTE_MER" "PC" 
														ON "PM"."IdProducto" = "PC"."IdProducto"
														)T2
													  ON T1."IdProducto" = T2."IdProducto"
													  WHERE T2."IdPedidos" = ${id}`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'inventory' : query.rows} 
			}else{
				data = { 'code' :201, 'inventory' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	/**
	@router http://localhost:3001/api/productos/mas/vendido
    **/
	public async  obtenerProductosMasVendido(): Promise<void> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT T1."TotalProd" "TotalProd",T1."NombreProd" "NombreProd", 
														T1."FachaPedido" "FachaPedido"
														FROM
														(SELECT  SUM(PC.cantidad) "TotalProd", PC."IdProducto", PC."FachaPedido", PM."NombreProd"
														FROM "PEDIDO_PRODUCTO_CLIENTE_MER" PC
														INNER JOIN "PRODUCTO_MER" PM ON PC."IdProducto" = PM."IdProducto"
														WHERE PC."FachaPedido" = '2020-03-01'
														GROUP BY PC."IdProducto", PM."NombreProd", PC."FachaPedido")T1
														ORDER BY T1."TotalProd" DESC LIMIT 1`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}
}