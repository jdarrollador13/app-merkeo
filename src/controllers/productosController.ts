import { Request, Response, NextFunction } from 'express'
import { Inject } from "typescript-ioc";
import { ProductosDAO } from '../DAO/ProductosDAO'



export default class ProductosController {
	constructor(
		@Inject private productosDAO: ProductosDAO,
	) {
	}

	async obtenerProductos(): Promise<void> {
		let res:any;
		try {
			let responseDao:any = await this.productosDAO.obtenerProductos()
			return responseDao
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

	async obtenerProductosConductor(): Promise<void> {
		let res:any;
		try {
			let responseDao:any = await this.productosDAO.obtenerProductosConductor()
			return responseDao
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

    async obtenerProductosMenosVendido(): Promise<void> {
		let res:any;
		try {
			let responseDao:any = await this.productosDAO.obtenerProductosMenosVendido()
			return responseDao
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}


    async obtenerProductosId(id:number|any): Promise<void> {
		let res:any;
		try {
			let responseDao:any = await this.productosDAO.obtenerProductosId(id)
			return responseDao
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}
	
	async obtenerProductosMasVendido(): Promise<void> {
		let res:any;
		try {
			let responseDao:any = await this.productosDAO.obtenerProductosMasVendido()
			return responseDao
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

}
