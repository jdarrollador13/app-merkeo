--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.26
-- Dumped by pg_dump version 9.4.26
-- Started on 2020-07-12 16:28:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- TOC entry 2051 (class 1262 OID 16430)
-- Name: merkeo; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE merkeo WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Colombia.1252' LC_CTYPE = 'Spanish_Colombia.1252';


ALTER DATABASE merkeo OWNER TO postgres;

\connect merkeo

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11855)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2054 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 176 (class 1259 OID 16445)
-- Name: CLIENTE_MER; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public."CLIENTE_MER" (
    "IdCliente" integer NOT NULL,
    "NombreCliente" text,
    "telCliente" text,
    "DirCliente" text,
    "CedCliente" text
);


ALTER TABLE public."CLIENTE_MER" OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16443)
-- Name: CLIENTE_MER_IdCliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."CLIENTE_MER_IdCliente_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CLIENTE_MER_IdCliente_seq" OWNER TO postgres;

--
-- TOC entry 2055 (class 0 OID 0)
-- Dependencies: 175
-- Name: CLIENTE_MER_IdCliente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."CLIENTE_MER_IdCliente_seq" OWNED BY public."CLIENTE_MER"."IdCliente";


--
-- TOC entry 182 (class 1259 OID 16506)
-- Name: CONDUCTORES_MER; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public."CONDUCTORES_MER" (
    "IdConductor" integer NOT NULL,
    "NombreConductor" text,
    "IdentificacionConductor" text,
    "Ciudad" text
);


ALTER TABLE public."CONDUCTORES_MER" OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16504)
-- Name: CONDUCTORES_MER_IdConductor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."CONDUCTORES_MER_IdConductor_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CONDUCTORES_MER_IdConductor_seq" OWNER TO postgres;

--
-- TOC entry 2056 (class 0 OID 0)
-- Dependencies: 181
-- Name: CONDUCTORES_MER_IdConductor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."CONDUCTORES_MER_IdConductor_seq" OWNED BY public."CONDUCTORES_MER"."IdConductor";


--
-- TOC entry 178 (class 1259 OID 16469)
-- Name: PEDIDO_MER; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public."PEDIDO_MER" (
    "IdPedido" integer NOT NULL,
    "NombrePedido" text,
    "telPedido" text,
    "DirPedido" text,
    "FachaPedido" date DEFAULT now()
);


ALTER TABLE public."PEDIDO_MER" OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16467)
-- Name: PEDIDO_MER_IdPedido_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PEDIDO_MER_IdPedido_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PEDIDO_MER_IdPedido_seq" OWNER TO postgres;

--
-- TOC entry 2057 (class 0 OID 0)
-- Dependencies: 177
-- Name: PEDIDO_MER_IdPedido_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PEDIDO_MER_IdPedido_seq" OWNED BY public."PEDIDO_MER"."IdPedido";


--
-- TOC entry 180 (class 1259 OID 16497)
-- Name: PEDIDO_PRODUCTO_CLIENTE_MER; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public."PEDIDO_PRODUCTO_CLIENTE_MER" (
    "IdPedidos" integer NOT NULL,
    "IdCliente" integer,
    "IdProducto" integer,
    cantidad integer,
    "FachaPedido" date DEFAULT now(),
    "IdConductor" integer,
    "FactNo" integer
);


ALTER TABLE public."PEDIDO_PRODUCTO_CLIENTE_MER" OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 16495)
-- Name: PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq" OWNER TO postgres;

--
-- TOC entry 2058 (class 0 OID 0)
-- Dependencies: 179
-- Name: PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq" OWNED BY public."PEDIDO_PRODUCTO_CLIENTE_MER"."IdPedidos";


--
-- TOC entry 174 (class 1259 OID 16433)
-- Name: PRODUCTO_MER; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE public."PRODUCTO_MER" (
    "IdProducto" integer NOT NULL,
    "NombreProd" text,
    "Precio" double precision,
    "Unidades" integer,
    "Fecha" date DEFAULT now()
);


ALTER TABLE public."PRODUCTO_MER" OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 16431)
-- Name: PRODUCTO_MER_IdProducto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."PRODUCTO_MER_IdProducto_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."PRODUCTO_MER_IdProducto_seq" OWNER TO postgres;

--
-- TOC entry 2059 (class 0 OID 0)
-- Dependencies: 173
-- Name: PRODUCTO_MER_IdProducto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."PRODUCTO_MER_IdProducto_seq" OWNED BY public."PRODUCTO_MER"."IdProducto";


--
-- TOC entry 1911 (class 2604 OID 16448)
-- Name: IdCliente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CLIENTE_MER" ALTER COLUMN "IdCliente" SET DEFAULT nextval('public."CLIENTE_MER_IdCliente_seq"'::regclass);


--
-- TOC entry 1916 (class 2604 OID 16509)
-- Name: IdConductor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."CONDUCTORES_MER" ALTER COLUMN "IdConductor" SET DEFAULT nextval('public."CONDUCTORES_MER_IdConductor_seq"'::regclass);


--
-- TOC entry 1912 (class 2604 OID 16472)
-- Name: IdPedido; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PEDIDO_MER" ALTER COLUMN "IdPedido" SET DEFAULT nextval('public."PEDIDO_MER_IdPedido_seq"'::regclass);


--
-- TOC entry 1914 (class 2604 OID 16500)
-- Name: IdPedidos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PEDIDO_PRODUCTO_CLIENTE_MER" ALTER COLUMN "IdPedidos" SET DEFAULT nextval('public."PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq"'::regclass);


--
-- TOC entry 1909 (class 2604 OID 16436)
-- Name: IdProducto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."PRODUCTO_MER" ALTER COLUMN "IdProducto" SET DEFAULT nextval('public."PRODUCTO_MER_IdProducto_seq"'::regclass);


--
-- TOC entry 2039 (class 0 OID 16445)
-- Dependencies: 176
-- Data for Name: CLIENTE_MER; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."CLIENTE_MER" ("IdCliente", "NombreCliente", "telCliente", "DirCliente", "CedCliente") VALUES (1, 'Jonathan Pinto', '3162542541', 'CALLE 45 12 09', '1109873744');
INSERT INTO public."CLIENTE_MER" ("IdCliente", "NombreCliente", "telCliente", "DirCliente", "CedCliente") VALUES (2, 'Carmen Lopez', '3178749106', 'Calle 23 12 09 sur', '252444222');


--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 175
-- Name: CLIENTE_MER_IdCliente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."CLIENTE_MER_IdCliente_seq"', 2, true);


--
-- TOC entry 2045 (class 0 OID 16506)
-- Dependencies: 182
-- Data for Name: CONDUCTORES_MER; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."CONDUCTORES_MER" ("IdConductor", "NombreConductor", "IdentificacionConductor", "Ciudad") VALUES (1, 'Jonathan Pinto', '10938838', 'Bogota');
INSERT INTO public."CONDUCTORES_MER" ("IdConductor", "NombreConductor", "IdentificacionConductor", "Ciudad") VALUES (2, 'Carolina Sanchez', '277227721', 'Bogota');


--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 181
-- Name: CONDUCTORES_MER_IdConductor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."CONDUCTORES_MER_IdConductor_seq"', 2, true);


--
-- TOC entry 2041 (class 0 OID 16469)
-- Dependencies: 178
-- Data for Name: PEDIDO_MER; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 177
-- Name: PEDIDO_MER_IdPedido_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PEDIDO_MER_IdPedido_seq"', 1, false);


--
-- TOC entry 2043 (class 0 OID 16497)
-- Dependencies: 180
-- Data for Name: PEDIDO_PRODUCTO_CLIENTE_MER; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."PEDIDO_PRODUCTO_CLIENTE_MER" ("IdPedidos", "IdCliente", "IdProducto", cantidad, "FachaPedido", "IdConductor", "FactNo") VALUES (1, 1, 1, 2, '2020-03-01', 1, 1010);
INSERT INTO public."PEDIDO_PRODUCTO_CLIENTE_MER" ("IdPedidos", "IdCliente", "IdProducto", cantidad, "FachaPedido", "IdConductor", "FactNo") VALUES (2, 1, 2, 2, '2020-03-01', 1, 1010);
INSERT INTO public."PEDIDO_PRODUCTO_CLIENTE_MER" ("IdPedidos", "IdCliente", "IdProducto", cantidad, "FachaPedido", "IdConductor", "FactNo") VALUES (3, 1, 3, 3, '2020-03-01', 1, 1010);
INSERT INTO public."PEDIDO_PRODUCTO_CLIENTE_MER" ("IdPedidos", "IdCliente", "IdProducto", cantidad, "FachaPedido", "IdConductor", "FactNo") VALUES (4, 2, 1, 2, '2020-03-01', 2, 1011);
INSERT INTO public."PEDIDO_PRODUCTO_CLIENTE_MER" ("IdPedidos", "IdCliente", "IdProducto", cantidad, "FachaPedido", "IdConductor", "FactNo") VALUES (5, 2, 4, 6, '2020-03-01', 2, 1011);


--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 179
-- Name: PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PEDIDO_PRODUCTO_CLIENTE_MER_IdPedidos_seq"', 5, true);


--
-- TOC entry 2037 (class 0 OID 16433)
-- Dependencies: 174
-- Data for Name: PRODUCTO_MER; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."PRODUCTO_MER" ("IdProducto", "NombreProd", "Precio", "Unidades", "Fecha") VALUES (1, 'Aceite 3000 mg', 17000, 10, '2020-07-12');
INSERT INTO public."PRODUCTO_MER" ("IdProducto", "NombreProd", "Precio", "Unidades", "Fecha") VALUES (2, 'Azucar 1Kg', 2400, 20, '2020-07-12');
INSERT INTO public."PRODUCTO_MER" ("IdProducto", "NombreProd", "Precio", "Unidades", "Fecha") VALUES (3, 'Arroz  500gr', 1500, 40, '2020-07-12');
INSERT INTO public."PRODUCTO_MER" ("IdProducto", "NombreProd", "Precio", "Unidades", "Fecha") VALUES (4, 'Alverja 500gr', 1700, 40, '2020-07-12');


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 173
-- Name: PRODUCTO_MER_IdProducto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."PRODUCTO_MER_IdProducto_seq"', 4, true);


--
-- TOC entry 1920 (class 2606 OID 16453)
-- Name: CLIENTE_MER_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public."CLIENTE_MER"
    ADD CONSTRAINT "CLIENTE_MER_pkey" PRIMARY KEY ("IdCliente");


--
-- TOC entry 1926 (class 2606 OID 16514)
-- Name: CONDUCTORES_MER_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public."CONDUCTORES_MER"
    ADD CONSTRAINT "CONDUCTORES_MER_pkey" PRIMARY KEY ("IdConductor");


--
-- TOC entry 1922 (class 2606 OID 16478)
-- Name: PEDIDO_MER_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public."PEDIDO_MER"
    ADD CONSTRAINT "PEDIDO_MER_pkey" PRIMARY KEY ("IdPedido");


--
-- TOC entry 1924 (class 2606 OID 16503)
-- Name: PEDIDO_PRODUCTO_CLIENTE_MER_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public."PEDIDO_PRODUCTO_CLIENTE_MER"
    ADD CONSTRAINT "PEDIDO_PRODUCTO_CLIENTE_MER_pkey" PRIMARY KEY ("IdPedidos");


--
-- TOC entry 1918 (class 2606 OID 16442)
-- Name: PRODUCTO_MER_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY public."PRODUCTO_MER"
    ADD CONSTRAINT "PRODUCTO_MER_pkey" PRIMARY KEY ("IdProducto");


--
-- TOC entry 2053 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-07-12 16:28:02

--
-- PostgreSQL database dump complete
--

