import { Request, Response, NextFunction, Router } from 'express'
import ProductosController from '../../controllers/productosController'
import { Container } from "typescript-ioc";


export default class routerProductos {
  public app: Router
  constructor(router: Router) {
    this.app = router
  }
  router(): void {
    this.app.get(
      '/productos/listado',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const productosController: ProductosController = Container.get(ProductosController);
          let responseModel = await productosController.obtenerProductos();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )

    this.app.get(
      '/productos/listado/conductor',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const productosController: ProductosController = Container.get(ProductosController);
          let responseModel = await productosController.obtenerProductosConductor();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )

    this.app.get(
      '/productos/menos/vendido',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const productosController: ProductosController = Container.get(ProductosController);
          let responseModel = await productosController.obtenerProductosMenosVendido();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )

    this.app.get(
      '/productos/listado/:id',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log(req.params.id)
          const productosController: ProductosController = Container.get(ProductosController);
          let responseModel = await productosController.obtenerProductosId(req.params.id);
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )
    
    
    this.app.get(
      '/productos/mas/vendido',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          const productosController: ProductosController = Container.get(ProductosController);
          let responseModel = await productosController.obtenerProductosMasVendido();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )
  }
}